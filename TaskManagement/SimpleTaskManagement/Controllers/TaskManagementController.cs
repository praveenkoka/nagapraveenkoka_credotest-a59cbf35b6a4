﻿using SimpleTaskManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SimpleTaskManagement.Controllers
{
    public class TaskManagementController : ApiController
    {
        ITaskRepository repository = new TaskRepository();

        public TaskManagementController()
        {

        }
        public TaskManagementController(ITaskRepository rep)
        {
            repository = rep;
        }
        
        /// <summary>
        /// Gets all the Tasks.
        /// </summary>
        /// <returns>A IEnumerable collection of type Task</returns>
        public IEnumerable<Task> GetAllTasks()
        {
            return repository.GetAll();
        }

        /// <summary>
        /// Saves a Task.
        /// </summary>
        /// <param name="task">Object of type Task which needed to be saved</param>
        /// <returns>The saved Object of type Task with an Id</returns>
        public Task PostTask(Task task)
        {
            task = repository.Add(task);
            return task;
        }

        /// <summary>
        /// Updates the status of the Task either completed or not
        /// </summary>
        /// <param name="taskID">An integer which the Id of the Task to be updated</param>
        /// <param name="task">An object of type Task that is to be updated</param>
        [Route("api/TaskManagement/{taskID:int}")]
        public void PutTask(int taskID, Task task)
        {

            if (!repository.Update(task))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

    }
}
