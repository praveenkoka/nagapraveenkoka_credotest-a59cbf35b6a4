﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaskManagement.Models
{
    public interface ITaskRepository
    {
        IEnumerable<Task> GetAll();
        Task Get(int taskID);
        Task Add(Task item);
        void Remove(int taskID);
        bool Update(Task task);
    }
}
