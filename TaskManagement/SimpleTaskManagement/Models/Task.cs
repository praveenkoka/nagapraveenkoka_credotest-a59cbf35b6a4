﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleTaskManagement.Models
{
    public class Task
    {
        public int Id { get; set; }
        public String Description { get; set; }
        public bool IsCompleted { get; set; }
    }
}