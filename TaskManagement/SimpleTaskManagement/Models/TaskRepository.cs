﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleTaskManagement.Models
{
    public class TaskRepository : ITaskRepository
    {
        
        public static List<Task> tasks = new List<Task>();
        public TaskRepository()
        {
            //tasks.Add(new Task {Id =1, Description ="Task1", IsCompleted = true });
            //Add(new Task { Id = 2, Description = "Task2", IsCompleted = false });
            //Add(new Task { Id = 3, Description = "Task3", IsCompleted = false });
            //Add(new Task { Id = 4, Description = "Task4", IsCompleted = true });
        }
        //private int _nextId = 1;

        public IEnumerable<Task> GetAll()
        {
            return tasks;
        }

        public Task Get(int taskID)
        {
            return tasks.Find(t => t.Id == taskID);
        }

        public Task Add(Task item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = tasks.Count+1;
            tasks.Add(item);
            return item;
        }

        public void Remove(int taskID)
        {
            tasks.RemoveAll(t => t.Id == taskID);
        }

        public bool Update(Task task)
        {
            if (task == null)
            {
                throw new ArgumentNullException("item");
            }
            Task currenttask = tasks.Find(x => x.Id == task.Id);
            currenttask.IsCompleted = task.IsCompleted;
              
            return true;
        }
    }
}