﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SimpleTaskManagement.Controllers;
using SimpleTaskManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTaskManagement.Tests.Controllers
{
    [TestClass]
    public class Test_TaskManagement
    {
        [TestMethod]
        public void Test_GetAllTasks()
        {
            //create list of tasks to test
            List<Models.Task> tasks = new List<Models.Task>();
            tasks.Add(new Models.Task { Id = 1, Description = "First Task", IsCompleted = true });
            tasks.Add(new Models.Task { Id = 2, Description = "Second Task", IsCompleted = false });

            Mock<ITaskRepository> test = new Mock<ITaskRepository>();
            test.Setup(a => a.GetAll()).Returns(tasks);

            var controller = new TaskManagementController(test.Object);

            Assert.AreEqual(true, controller.GetAllTasks().Count() == 2);
        }

        [TestMethod]
        public void Test_SaveTask()
        {
            //create new Task to test save
            var task = new Models.Task { Id = 0, Description = "New Task", IsCompleted = false };
           
            var controller = new TaskManagementController();
            var actual = controller.PostTask(task);

            var tasks = controller.GetAllTasks();
            var expected = tasks.LastOrDefault();

            Assert.AreEqual(true, expected.Id == actual.Id);
        }

        [TestMethod]
        public void Test_UpdateCompletedStatusToFALSE()
        {
            //create list of tasks to test
            List<Models.Task> tasks = new List<Models.Task>();
            tasks.Add(new Models.Task { Id = 1, Description = "Task to update", IsCompleted = true });

            var controller = new TaskManagementController();
            
            controller.PostTask(tasks[0]);

            var task = controller.GetAllTasks().FirstOrDefault();
            task.IsCompleted = false;

            controller.PutTask(task.Id, task);

            Assert.AreEqual(false, controller.GetAllTasks().FirstOrDefault().IsCompleted);
        }

        [TestMethod]
        public void Test_UpdateCompletedStatusToTRUE()
        {
            //create list of tasks to test
            List<Models.Task> tasks = new List<Models.Task>();
            tasks.Add(new Models.Task { Id = 1, Description = "Task to update", IsCompleted = false });

            var controller = new TaskManagementController();

            controller.PostTask(tasks[0]);

            var task = controller.GetAllTasks().FirstOrDefault();
            task.IsCompleted = true;

            controller.PutTask(task.Id, task);

            Assert.AreEqual(true, controller.GetAllTasks().FirstOrDefault().IsCompleted);
        }
    }
}
